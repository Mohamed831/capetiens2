

// hamburger menu

var btn = document.querySelector('.toggle-btn');
var nav = document.querySelector('.nav');

btn.onclick = function () {
    nav.classList.toggle('nav_open');
}

///


window.onscroll = function () { myFunction() };

var header = document.getElementById("header");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

var slides_container = document.getElementById('box');

var image = ['auguste-02', 'capet-01', 'capet-02', 'le-bel-02'];

var i = image.length;

function nextImage() {
    if (i < image.length) {
        i = i + 1;

    } else {
        i = 1;
    }
    slides_container.innerHTML = "<img src =" + image[i - 1] + ".jpg>";
}

    function prevImage() {
        if (i < image.length + 1 && i > 1) {
            i = i - 1;

        } else {
            i = image.length;
        }
        slides_container.innerHTML = "<img src =" + image[i - 1] + ".jpg>";
    }
    

setInterval(nextImage, 2000);

///

function grand(font) {
    font.style.fontSize = "2em";
    font.style.color = "red";

}

function normal(font) {
    font.style.fontSize = "1em";
    font.style.color = "rgb(10, 9, 9)";

}

// clock
setInterval(runClock, 1000);
function runClock() {
    var now = new Date();
    var hrs = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    if (hrs < 10) {
        hrs = '0' + hrs;
    }
    if (min < 10) {
        min = '0' + min;
    }
    if (sec < 10) {
        sec = '0' + sec;
    }
    if (min % 2 == 0) {
        document.getElementById('time').style.color = "green";
    } else {
        document.getElementById('time').style.color = "blue";
    }
    document.getElementById('time').innerHTML = hrs + ':' + min + ':' + sec;
}


// Diaporama1

var i = 0;

var images = [];



images[0] = 'louis-02.jpg';
images[1] = 'auguste-02.jpg';
images[2] = 'capet-01.jpg';
images[3] = 'le bel1.jpg';


function changeImg() {

    document.slide.src = images[i];
    if (i < images.length - 1) {
        i++;
    } else {
        i = 0;
    }
    setTimeout("changeImg()", 2000);
}
window.onload = changeImg;