


var btn = document.querySelector('.toggle-btn');
var nav = document.querySelector('.nav');

btn.onclick = function () {
    nav.classList.toggle('nav_open');
}

///


window.onscroll = function () { myFunction() };

var header = document.getElementById("header");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}


function grand(font) {
    font.style.fontSize = "2em";
    font.style.color = "red";

}

function normal(font) {
    font.style.fontSize = "1em";
    font.style.color = "rgb(15, 11, 65)";

}

// clock
setInterval(runClock, 1000);
function runClock() {
    var now = new Date();
    var hrs = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    if (hrs < 10) {
        hrs = '0' + hrs;
    }
    if (min < 10) {
        min = '0' + min;
    }
    if (sec < 10) {
        sec = '0' + sec;
    }
    if (min % 2 == 0) {
        document.getElementById('time').style.color = "green";
    } else {
        document.getElementById('time').style.color = "blue";
    }
    document.getElementById('time').innerHTML = hrs + ':' + min + ':' + sec;
}
