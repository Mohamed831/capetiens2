



//

const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname +'/homepage.html'));
  //__dirname : It will resolve to your project folder.
});

router.get('/lebel',function(req,res){
  res.sendFile(path.join(__dirname+'/html-files/lebel.html'));
});


router.get('/auguste', function (req, res) {
    res.sendFile(path.join(__dirname + '/auguste.html'));
});


router.get('/capet', function (req, res) {
    res.sendFile(path.join(__dirname + '/capet.html'));
});

router.get('/louis',function(req,res){
  res.sendFile(path.join(__dirname+'/louis.html'));
});


router.get('/diaporama', function (req, res) {
    res.sendFile(path.join(__dirname + '/diaporama.html'));
});

router.get('/contact', function (req, res) {
  res.sendFile(path.join(__dirname + '/form.html'));
});
//add the router
app.use(express.static(__dirname + '/html-files'));
//Store all HTML files in view folder.
app.use(express.static(__dirname + '/Medias'));
//Store all JS and CSS in Scripts folder.

app.use('/', router);
app.listen(process.env.port || 3000);

console.log('Your Server is Running at Port 3000');